use tests::*;

#[test]
fn password_simple() {
    test_foldered_program("../password-simple")
}

#[test]
fn simple_example() {
    test_foldered_program("../simple-example")
}

#[test]
fn fibinacci() {
    test_foldered_program("../fibinacci")
}
